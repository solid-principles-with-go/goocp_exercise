// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          primos.go                             */
/* --                                                                */
/* --  Descripcion: Clase que mostrara los numeros primos.           */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package primos

func (p Primos) GeneratePrimos() []int32 {
	var X []int32
	for i := 2; i <= p.N; i++ {
		if isPrimo(i) {
			X = append(X, int32(i))
		}
	}

	return X
}

func isPrimo(num int) bool {
	for j := 2; j < num; j++ {
		if num%j == 0 {
			return false
		}
	}
	return true
}
