// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                        primos_test.go                          */
/* --                                                                */
/* --  Descripcion: Contiene las pruebas unitarias del ejercicios    */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package test

import (
	"testing"

	"gitlab.com/rodrigoghm/goocp_ejercicio/class/primos"
	"gitlab.com/rodrigoghm/goocp_ejercicio/class/utils"
)

func TestGeneratePrimos_ordenNatural(t *testing.T) {
	var Pri primos.Primos
	Pri.N = 17
	J := Pri.GeneratePrimos()
	expected := []int32{2, 3, 5, 7, 11, 13, 17}

	if !utils.EqualSlices(J, expected) {
		t.Errorf("Valor esperado en Orden Natural incorrecto!")
	}
}

func TestGeneratePrimos_ordenInverso(t *testing.T) {
	var Pri primos.Primos
	Pri.N = 21
	J := Pri.GeneratePrimos()
	expected := []int32{19, 17, 13, 11, 7, 5, 3, 2}

	if !utils.EqualSlices(J, expected) {
		t.Errorf("Valor esperado en Orden Inverso incorrecto!")
	}
}
